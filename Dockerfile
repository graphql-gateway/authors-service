FROM openjdk:13-alpine
RUN apk update; apk --no-cache add curl
LABEL maintainer="Maintainer"
VOLUME /tmp
ARG JAR_FILE=build/libs/demo.jar
ADD ${JAR_FILE} demo-api.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/demo-api.jar"]

package com.example.demo.controller;

import com.example.demo.dto.Author;
import java.util.Arrays;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

  @GetMapping("/authors")
  public List<Author> getBooks() {
    Author book1 = Author.builder().name("Author 1").age(35).build();
    Author book2 = Author.builder().name("Author 2").age(56).build();
    return Arrays.asList(book1, book2);
  }

}
